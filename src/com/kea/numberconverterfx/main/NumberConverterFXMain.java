/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kea.numberconverterfx.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.*;
import javafx.stage.Stage;

/**
 * @author Nicolai
 */
public class NumberConverterFXMain extends Application
{
    @Override
    public void start(Stage primaryStage)
    {
        Font font = Font.font("Arial", FontWeight.BOLD, 24);
        Font font1 = Font.font("Arial", FontWeight.BOLD, 16);
        Font font2 = Font.font("Arial", FontWeight.BOLD, 20);
        
        Label binLabel = new Label("BIN");
        binLabel.setFont(font);
        Label decLabel = new Label("DEC");
        decLabel.setFont(font);
        Label hexLabel = new Label("HEX");
        hexLabel.setFont(font);
        TextField binTF = new TextField();
        binTF.setFont(font1);
        TextField decTF = new TextField();
        decTF.setFont(font1);
        TextField hexTF = new TextField();
        hexTF.setFont(font1);
        Button btn = new Button();
        btn.setFont(font2);
        btn.setText("Reset");
        ImageView iv = new ImageView("/com/kea/numberconverterfx/main/bg1.jpg");
        
        btn.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                binTF.setText("");
                decTF.setText("");
                hexTF.setText("");
            }
        });
        
        binTF.setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                String input = binTF.getText();
                int lastCharIndex = input.length()-1;
                char lastChar = input.charAt(lastCharIndex);
                if (!(lastChar == '0' || lastChar == '1'))
                {
                    input = input.substring(0, lastCharIndex);
                    binTF.setText(input);
                }
                int dec = Integer.parseInt(input, 2);
                decTF.setText(dec+"");
                String hexString = Integer.toHexString(dec);
                hexTF.setText(hexString);
            }
        });
        
        decTF.setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                String input = decTF.getText();
                int lastCharIndex = input.length()-1;
                char lastChar = input.charAt(lastCharIndex);
                if (!(lastChar > 47 && lastChar < 58))
                {
                    input = input.substring(0, lastCharIndex);
                    decTF.setText(input);
                }
                int bin = Integer.parseInt(input);
                hexTF.setText(Integer.toHexString(bin));
                binTF.setText(Integer.toBinaryString(bin));
            }
        });
        
        hexTF.setOnKeyReleased(new EventHandler<KeyEvent>()
        {
            @Override
            public void handle(KeyEvent event)
            {
                String input = hexTF.getText();
                int lastCharIndex = input.length()-1;
                char lastChar = input.charAt(lastCharIndex);
                if (!(lastChar > 47 && lastChar < 58) && !(lastChar > 64 && lastChar < 71) && !(lastChar > 96 && lastChar < 103))
                {
                    input = input.substring(0, lastCharIndex);
                    hexTF.setText(input);
                }
                Integer hex = Integer.parseInt(input, 16);
                decTF.setText(hex.toString());
                binTF.setText(Integer.toBinaryString(hex));
            }
        });
        
        GridPane acPane = new GridPane();
        GridPane gPane = new GridPane();
        acPane.setAlignment(Pos.CENTER);
        gPane.setVgap(20);
        gPane.setHgap(10);
        gPane.setAlignment(Pos.CENTER);
        gPane.getChildren().addAll(binLabel, decLabel, hexLabel, binTF, decTF, hexTF, btn);
        acPane.getChildren().addAll(iv, gPane);
        GridPane.setConstraints(binLabel, 0, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(decLabel, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(hexLabel, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(binTF, 1, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(decTF, 1, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(hexTF, 1, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setConstraints(btn, 1, 3, 1, 1, HPos.RIGHT, VPos.BOTTOM);
        
        Scene scene = new Scene(acPane, 300, 300);
        
        primaryStage.setTitle("NumberConverter");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        launch(args);
    }
    
}
